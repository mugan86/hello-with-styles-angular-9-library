/*
 * Public API Surface of hello-world-styles
 */

export * from './lib/hello-world-styles.service';
export * from './lib/hello-world-styles.component';
export * from './lib/hello-world-styles.module';
