import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelloWorldStylesComponent } from './hello-world-styles.component';

describe('HelloWorldStylesComponent', () => {
  let component: HelloWorldStylesComponent;
  let fixture: ComponentFixture<HelloWorldStylesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelloWorldStylesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelloWorldStylesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
