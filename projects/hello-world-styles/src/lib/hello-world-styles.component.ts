import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'hws-hello-world-styles',
  template: `
    <p>
      hello-world-styles works!
    </p>
    <h1>Hola Anartz</h1>
  `,
  styles: [ '{ h2 { color: green;}}']
})
export class HelloWorldStylesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
