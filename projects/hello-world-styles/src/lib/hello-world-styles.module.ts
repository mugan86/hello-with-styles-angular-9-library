import { NgModule } from '@angular/core';
import { HelloWorldStylesComponent } from './hello-world-styles.component';



@NgModule({
  declarations: [HelloWorldStylesComponent],
  imports: [
  ],
  exports: [HelloWorldStylesComponent]
})
export class HelloWorldStylesModule { }
