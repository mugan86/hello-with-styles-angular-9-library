import { TestBed } from '@angular/core/testing';

import { HelloWorldStylesService } from './hello-world-styles.service';

describe('HelloWorldStylesService', () => {
  let service: HelloWorldStylesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HelloWorldStylesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
