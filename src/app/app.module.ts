import { HelloWorldStylesModule } from 'hello-world-styles-angular';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HelloWorldStylesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
